module.exports = [{
    entry: './style.sass',
    output: {
        // This is necessary for webpack to compile
        // But we never use style-bundle.js
        filename: 'style-bundle.js',
    },
    module: {
        rules: [{
            test: /\.sass$/,
            use: [{
                    loader: 'file-loader',
                    options: {
                        name: 'bundle.css',
                    },
                },
                {
                    loader: 'extract-loader'
                },
                {
                    loader: 'css-loader'
                },
                {
                    loader: 'sass-loader'
                },
            ]
        }]
    },
}];